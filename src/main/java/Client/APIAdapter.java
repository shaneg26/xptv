package Client;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import javax.json.JsonObject;
import java.io.IOException;
import java.io.InputStream;

public class APIAdapter {

    public CloseableHttpClient getCloseableHttpClient() {
        return HttpClients.createDefault();
    }

    public CloseableHttpResponse httpGet(final String url) {

        CloseableHttpResponse response = null;
        HttpGet httpGet = new HttpGet(url);
        try {
            response = this.getCloseableHttpClient().execute(httpGet);
//            System.out.println(response.getEntity().isRepeatable());
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public Object JSONtoJava(CloseableHttpResponse response) {

        JsonObject obj = null;

//        try (InputStream is = response.getEntity().getContent();
//             JsonReader rdr = Json.createReader(is)) {
//            obj = rdr.readObject();
////            JsonArray results = obj.getJsonArray("data");
////            obj.values();
//            Set<String> keySet = obj.keySet();
//            for (String key : keySet) {
//                System.out.println(key);
//                System.out.println(obj.get(key));
////                obj.get(key).asJsonArray().getJsonString(1);
////                System.out.println(obj.getJsonArray(key).get(0).toString());
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        return "null";
    }

}
