package Client.ModelEntities;

public class Departure {
    int stop_id;        // (integer, optional): Stop identifier ,
    int route_id;       // (integer, optional): Route identifier ,
    int run_id;         // (integer, optional): Trip/service run identifier ,
    int direction_id;           // (integer, optional): Direction of travel identifier ,
    int[] disruption_ids;       // (Array[integer], optional): Disruption information identifier(s) ,
    String scheduled_departure_utc;     // (string, optional): Scheduled (i.e. timetabled) departure time and date in ISO 8601 UTC format ,
    String estimated_departure_utc;     // (string, optional): Real-time estimate of departure time and date in ISO 8601 UTC format ,
    boolean at_platform;            // (boolean, optional): Indicates if the metropolitan train service is at the platform at the time of query; returns false for other modes ,
    String platform_number;         // (string, optional): Platform number at stop (metropolitan train only; returns null for other modes) ,
    String flags;                   // (string, optional): Flag indicating special condition for run (e.g. RR Reservations Required, GC Guaranteed Connection, DOO Drop Off Only, PUO Pick Up Only, MO Mondays only, TU Tuesdays only, WE Wednesdays only, TH Thursdays only, FR Fridays only, SS School days only; ignore E flag) ,
    String departure_sequence;      // (integer, optional): Chronological sequence of the departure for the run on the route. Order ascendingly by this field to get chronological order (earliest first) of departures with the same route_id and run_id.


    public Departure() {}

}
