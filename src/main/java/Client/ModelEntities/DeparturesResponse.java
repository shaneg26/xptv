package Client.ModelEntities;

public class DeparturesResponse {

    Departure[] departures; // (Array[V3.Departure], optional): Timetabled and real-time service departures ,
    Object stops;           // (object, optional): A train station, tram stop, bus stop, regional coach stop or Night Bus stop ,
    Object routes;          // (object, optional): Train lines, tram routes, bus routes, regional coach routes, Night Bus routes ,
    Object runs;            // (object, optional): Individual trips/services of a route ,
    Object directions;      // (object, optional): Directions of travel of route ,
    Object disruptions;     // (object, optional): Disruption information applicable to relevant routes or stops ,
    Status status;          // (V3.Status, optional): API Status / Metadata
}
