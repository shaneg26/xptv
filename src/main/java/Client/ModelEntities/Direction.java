package Client.ModelEntities;

public class Direction {
    int direction_id;       // (integer, optional): Direction of travel identifier ,
    String direction_name;  // (string, optional): Name of direction of travel ,
    int route_id;           // (integer, optional): Route identifier ,
    int route_type;         // (integer, optional): Transport mode identifier
}
