package Client.ModelEntities;

public class DirectionsResponse {
    Direction[] directions; // (Array[V3.Direction], optional): Directions of travel of route ,
    Status status;          // (V3.Status, optional): API Status / Metadata
}
