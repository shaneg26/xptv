package Client.ModelEntities;

/**
 * Created by Laeg on 13/7/18.
 */
public class Disruption {
    int disruption_id;  // (integer, optional): Disruption information identifier ,
    String title;       // (string, optional): Headline title summarising disruption information ,
    String url;         // (string, optional): URL of relevant article on PTV website ,
    String description; // (string, optional): Description of the disruption ,
    String disruption_status;   // (string, optional): Status of the disruption (e.g. "Planned", "Current") ,
    String disruption_type;     // (string, optional): Type of disruption ,
    String published_on;    // (string, optional): Date and time disruption information is published on PTV website, in ISO 8601 UTC format ,
    String last_updated;    // (string, optional): Date and time disruption information was last updated by PTV, in ISO 8601 UTC format ,
    String from_date;       // (string, optional): Date and time at which disruption begins, in ISO 8601 UTC format ,
    String to_date;         // (string, optional): Date and time at which disruption ends, in ISO 8601 UTC format (returns null if unknown) ,
    DisruptionRoute[] routes;   // (Array[V3.DisruptionRoute], optional): Route relevant to a disruption (if applicable)
}
