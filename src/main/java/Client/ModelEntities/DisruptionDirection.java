package Client.ModelEntities;

public class DisruptionDirection {
    int route_direction_id; // (integer, optional): Route and direction of travel combination identifier ,
    int direction_id;       // (integer, optional): Direction of travel identifier ,
    String direction_name;  // (string, optional): Name of direction of travel ,
    String service_time;    // (string, optional): Time of service to which disruption applies, in 24 hour clock format (HH:MM:SS) AEDT/AEST; returns null if disruption applies to multiple (or no) services
}
