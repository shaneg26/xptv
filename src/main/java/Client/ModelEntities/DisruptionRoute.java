package Client.ModelEntities;

public class DisruptionRoute {
    int route_type; // (integer, optional): Transport mode identifier ,
    int route_id;   // (integer, optional): Route identifier ,
    String route_name;  // (string, optional): Name of route ,
    String route_number;    // (string, optional): Route number presented to public (i.e. not route_id) ,
    String route_gtfs_id;   // (string, optional): GTFS Identifer of the route ,
    DisruptionDirection direction;  // (V3.DisruptionDirection, optional): Direction of travel relevant to a disruption (if applicable)
}
