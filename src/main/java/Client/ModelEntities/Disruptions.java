package Client.ModelEntities;

public class Disruptions {
    Disruption[] general;       // (Array[V3.Disruption], optional): Subset of disruption information applicable to multiple route_types ,
    Disruption[] metro_train;   // (Array[V3.Disruption], optional): Subset of disruption information applicable to metropolitan train ,
    Disruption[] metro_tram;    // (Array[V3.Disruption], optional): Subset of disruption information applicable to metropolitan tram ,
    Disruption[] metro_bus;     // (Array[V3.Disruption], optional): Subset of disruption information applicable to metropolitan bus ,
    Disruption[] regional_train;// (Array[V3.Disruption], optional): Subset of disruption information applicable to V/Line train ,
    Disruption[] regional_coach;//(Array[V3.Disruption], optional): Subset of disruption information applicable to V/Line coach ,
    Disruption[] regional_bus;  // (Array[V3.Disruption], optional): Subset of disruption information applicable to regional bus
}
