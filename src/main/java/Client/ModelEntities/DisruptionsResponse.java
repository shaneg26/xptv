package Client.ModelEntities;

public class DisruptionsResponse {
    Disruptions disruptions;    // (V3.Disruptions, optional): Disruption information applicable to relevant routes or stops ,
    Status status;              // (V3.Status, optional): API Status / Metadata
}
