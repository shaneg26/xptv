package Client.ModelEntities;

/**
 * Created by Laeg on 13/7/18.
 */
public class Outlet {
    String outlet_slid_spid;    // (string, optional): The SLID / SPID ,
    String outlet_name;         // (string, optional): The location name of the outlet ,
    String outlet_business;     // (string, optional): The business name of the outlet ,
    double outlet_latitude;     // (number, optional): Geographic coordinate of latitude at outlet ,
    double outlet_longitude;    // (number, optional): Geographic coordinate of longitude at outlet ,
    String outlet_suburb;       // (string, optional): The city/municipality the outlet is in ,
    int outlet_postcode;        // (integer, optional): The postcode for the outlet ,
    String outlet_business_hour_mon;    // (string, optional): The business hours on Monday ,
    String outlet_business_hour_tue;    // (string, optional): The business hours on Tuesday ,
    String outlet_business_hour_wed;    // (string, optional): The business hours on Wednesday ,
    String outlet_business_hour_thur;   // (string, optional): The business hours on Thursday ,
    String outlet_business_hour_fri;    // (string, optional): The business hours on Friday ,
    String outlet_business_hour_sat;    // (string, optional): The business hours on Saturday ,
    String outlet_business_hour_sun;    // (string, optional): The business hours on Sunday ,
    String outlet_notes;    // (string, optional): Any additional notes for the outlet such as 'Buy pre-loaded myki cards only'. May be null/empty.
}
