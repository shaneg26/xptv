package Client.ModelEntities;

public class OutletGeolocationResponse {
    OutletGeolocation[] outlets;    // (Array[V3.OutletGeolocation], optional): myki ticket outlets ,
    Status status;                  // (V3.Status, optional): API Status / Metadata
}
