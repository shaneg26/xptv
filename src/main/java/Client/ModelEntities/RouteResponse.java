package Client.ModelEntities;

public class RouteResponse {
    Route route;    // (V3.Route, optional): Train lines, tram routes, bus routes, regional coach routes, Night Bus routes ,
    Status status;  // (V3.Status, optional): API Status / Metadata
}
