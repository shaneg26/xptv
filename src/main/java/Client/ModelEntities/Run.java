package Client.ModelEntities;

public class Run {
    int run_id;         // (integer, optional): Trip/service run identifier ,
    int route_id;       // (integer, optional): Route identifier ,
    int route_type;     // (integer, optional): Transport mode identifier ,
    int final_stop_id;      // (integer, optional): stop_id of final stop of run ,
    String destination_name;// (string, optional): Name of destination of run ,
    String status;      // (string, optional): Status of metropolitan train run; returns "scheduled" for other modes ,
    int direction_id;   // (integer, optional): Direction of travel identifier ,
    int run_sequence;   // (integer, optional): Chronological sequence of the trip/service run on the route in direction. Order ascendingly by this field to get chronological order (earliest first) of runs with the same route_id and direction_id. ,
    int express_stop_count;     // (integer, optional),
    VehiclePosition vehicle_position;       // (V3.VehiclePosition, optional): Position of the trip/service run. Only available for some bus runs. May be null. ,
    VehicleDescriptor vehicle_descriptor;   // (V3.VehicleDescriptor, optional): Descriptor of the trip/service run. Only available for some runs. May be null.
}
