package Client.ModelEntities;

public class RunsResponse {
    Run[] runs;     // (Array[V3.Run], optional): Individual trips/services of a route ,
    Status status;  // (V3.Status, optional): API Status / Metadata
}
