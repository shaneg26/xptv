package Client.ModelEntities;

public class SearchResult {
    ResultStop[] stops;     // (Array[V3.ResultStop], optional): Train stations, tram stops, bus stops, regional coach stops or Night Bus stops ,
    ResultRoute[] routes;   // (Array[V3.ResultRoute], optional): Train lines, tram routes, bus routes, regional coach routes, Night Bus routes ,
    ResultOutlet[] outlets; // (Array[V3.ResultOutlet], optional): myki ticket outlets ,
    Status status;          // (V3.Status, optional): API Status / Metadata
}
