package Client.ModelEntities;

public class StopAccessibility {
    boolean lighting;   // (boolean, optional): Indicates if there is lighting at the stop ,
    boolean stairs;     // (boolean, optional): Indicates if there are stairs at the stop ,
    boolean escalator;  // (boolean, optional): Indicates if there is an escalator at the stop ,
    boolean lifts;      // (boolean, optional): Indicates if there is an elevator at the stop ,
    boolean hearing_loop;   // (boolean, optional): Indicates if there is a hearing loop facility at the stop ,
    boolean tactile_tiles;  // (boolean, optional): Indicates if there are tactile tiles (also known as tactile ground surface indicators, or TGSIs) at the stop ,
    StopAccessibilityWheelchair wheelchair; // (V3.StopAccessibilityWheelchair, optional): Facilities relating to the accessibility of the stop by wheelchair
}
