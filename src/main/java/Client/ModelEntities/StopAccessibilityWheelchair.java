package Client.ModelEntities;

public class StopAccessibilityWheelchair {
    boolean accessible_ramp;    // (boolean, optional): Indicates if there is a ramp at the stop that complies with the Disability Standards for Accessible Public Transport under the Disability Discrimination Act (1992) ,
    boolean accessible_parking; // (boolean, optional): Indicates if there is at least one accessible parking spot at the stop that complies with the Disability Standards for Accessible Public Transport under the Disability Discrimination Act (1992) ,
    boolean accessible_phone;   // (boolean, optional): Indicates if there is an accessible public telephone at the stop that complies with the Disability Standards for Accessible Public Transport under the Disability Discrimination Act (1992) ,
    boolean accessible_toilet;  // (boolean, optional): Indicates if there is an accessible public toilet at the stop that complies with the Disability Standards for Accessible Public Transport under the Disability Discrimination Act (1992)
}
