package Client.ModelEntities;

public class StopAmenityDetails {
    boolean toilet;     // (boolean, optional): Indicates if there is a public toilet at or near the stop ,
    boolean taxi_rank;  // (boolean, optional): Indicates if there is a taxi rank at or near the stop ,
    String car_parking; // (string, optional): The number of free car parking spots at the stop ,
    boolean cctv;       // (boolean, optional): Indicates if there are CCTV (i.e. closed circuit television) cameras at the stop
}
