package Client.ModelEntities;

public class StopDetails {
    String station_type;        // (string, optional): Type of metropolitan train station (i.e. "Premium", "Host" or "Unstaffed" station); returns null for V/Line train ,
    String station_description; // (string, optional): The definition applicable to the station_type; returns null for V/Line train ,
    int route_type;             // (integer, optional): Transport mode identifier ,
    StopLocation stop_location; // (V3.StopLocation, optional): Location details of the stop ,
    StopAmenityDetails stop_amenities;      // (V3.StopAmenityDetails, optional): Amenity and facility details at the stop ,
    StopAccessibility stop_accessibility;   // (V3.StopAccessibility, optional): Facilities relating to the accessibility of the stop ,
    int stop_id;        // (integer, optional): Stop identifier ,
    String stop_name;   // (string, optional): Name of stop
}
