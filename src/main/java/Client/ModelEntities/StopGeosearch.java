package Client.ModelEntities;

public class StopGeosearch {
    double  stop_distance;  // (number, optional): Distance of stop from input location (in metres); returns 0 if no location is input ,
    String stop_suburb;     // (string, optional): suburb of stop ,
    String stop_name;       // (string, optional): Name of stop ,
    int stop_id;            // (integer, optional): Stop identifier ,
    int route_type;         // (integer, optional): Transport mode identifier ,
    double stop_latitude;   // (number, optional): Geographic coordinate of latitude at stop ,
    double stop_longitude;  // (number, optional): Geographic coordinate of longitude at stop ,
    int stop_sequence;      // (integer, optional): Sequence of the stop on the route/run; return 0 when route_id or run_id not specified. Order ascendingly by this field (when non zero) to get physical order (earliest first) of stops on the route_id/run_id.
}
