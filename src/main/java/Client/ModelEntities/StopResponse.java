package Client.ModelEntities;

public class StopResponse {
    StopDetails stop;   // (V3.StopDetails, optional): A metropolitan or V/Line train station ,
    Status status;      // (V3.Status, optional): API Status / Metadata
}
