package Client.ModelEntities;

public class StopsByDistanceResponse {
    StopGeosearch[] stops;  // (Array[V3.StopGeosearch], optional): Train stations, tram stops, bus stops, regional coach stops or Night Bus stops ,
    Status status;          // (V3.Status, optional): API Status / Metadata
}
