package Client.ModelEntities;

public class StopsOnRouteResponse {
    StopOnRoute[] stops;  // (Array[V3.StopOnRoute], optional): Train stations, tram stops, bus stops, regional coach stops or Night Bus stops ,
    Status status;      // (V3.Status, optional): API Status / Metadata
}
