package Client.ModelEntities;

public class VehicleDescriptor {
    String operator;    // (string, optional): Operator name of the vehicle such as "Metro Trains Melbourne", "Yarra Trams", "Ventura Bus Line", "CDC" or "Sita Bus Lines" . May be null/empty. Only available for train, tram, v/line and some bus runs. ,
    String id;          // (string, optional): Operator identifier of the vehicle such as "26094". May be null/empty. Only available for some tram and bus runs. ,
    boolean low_floor;  // (boolean, optional): Indicator if vehicle has a low floor. May be null. Only available for some tram runs. ,
    boolean air_conditioned;    // (boolean, optional): Indicator if vehicle is air conditioned. May be null. Only available for some tram runs. ,
    String description; // (string, optional): Vehicle description such as "6 Car Comeng", "6 Car Xtrapolis", "3 Car Comeng", "6 Car Siemens", "3 Car Siemens". May be null/empty. Only available for some metropolitan train runs. ,
    String supplier;    // (string, optional, read only): Supplier of vehicle descriptor data.
}
