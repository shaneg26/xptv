package Client.ModelEntities;

public class VehiclePosition {
    double latitude;    // (number, optional): Geographic coordinate of latitude of the vehicle when known. May be null. Only available for some bus runs. ,
    double longitude;   // (number, optional): Geographic coordinate of longitude of the vehicle when known. Only available for some bus runs. ,
    double bearing;     // (number, optional): Compass bearing of the vehicle when known, clockwise from True North, i.e., 0 is North and 90 is East. May be null. Only available for some bus runs. ,
    String supplier;    // (string, optional, read only): Supplier of vehicle position data.
}
