package Client;

import Client.APIAdapter;
import Client.ResponseInterpreter;
import Client.UrlBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class PTVClient {
    private static int userId = 3000660;
    private static String apiKey= "fdefa9ff-5e4f-4d96-8e0e-a43c1882cf9c";

    private String latitude;
    private String longitude;

    public static void main(String[] args) throws IOException {
        final String baseURL = "http://timetableapi.ptv.vic.gov.au";
        final String uri = "/v3/search/Flinders";
//        final String uri = "/v3/routes?route_types=0";
//        final String uri = "/v3/runs/route/725?";
//        final String uri = "/v3/stops/location/-37.915841,145.038172";
//        final String uri = "/v3/stops/location/-37.824085,144.962332/route_types=0";

//        final String uri = UrlBuilder.buildUri();

        // Build the request url.
        String url = UrlBuilder.buildTTAPIURL(baseURL, uri, userId, apiKey);

        APIAdapter apiAd = new APIAdapter();
        CloseableHttpResponse response = null;
        Scanner s = null;

        try {
            response = apiAd.httpGet(url);
//            s = new Scanner(response.getEntity().getContent()).useDelimiter("\\A");
//
//            String result = s.hasNext() ? s.next() : "";
//            System.out.println(result);
            System.out.println(EntityUtils.toString(response.getEntity()));

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            response.close();
        }

    }


    public String interpretResponse(CloseableHttpResponse response) {
        return ResponseInterpreter.interpretResponse(response);
    }

}
